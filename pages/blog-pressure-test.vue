<template>
  <slp-blog-layout>
    <template v-slot:header>
      <slp-breadcrumbs
        :base="breadcrumbs.base"
        :category="breadcrumbs.category"
      />
      <slp-decoration modifier="gitops-header-bg-2" class="slp-transform" />
      <slp-tags :links="tagsLinks" />
      <slp-blog-header
        :author="header.author"
        :date="header.date"
        :readTime="header.readTime"
        :title="header.title"
        class="slp-mx-auto"
      />
    </template>
    <template v-slot:article>
      <div class="content">
        <h2>
          Roles are changing and AI is coming. We asked 14 DevOps practitioners,
          analysts, and GitLab execs how to future-proof your career.
        </h2>
        <p>
          <em
            >This is the fourth and final part of our series on the future of
            software development. Part one examined
            <a href="/blog/2020/10/20/software-developer-changing-role/"
              >how the software developer role is changing</a
            >. Part two highlighted
            <a href="/blog/2020/10/21/how-tomorrows-tech-affects-sw-dev/"
              >“future” technologies likely to impact the way software is
              created</a
            >. Part three looked at
            <a href="/blog/2020/10/28/ai-in-software-development/"
              >the role artificial intelligence (AI) will play in software
              development</a
            >.</em
          >
        </p>
        <p>
          Changing roles, emerging technologies, and the promise (or threat) of
          artificial intelligence are colliding, creating a critical question
          for software developers: how should you future-proof your career?
        </p>
        <p>
          Anyone in the technology industry knows change is both swift and
          expected – remember
          <a href="https://www.investopedia.com/terms/m/mooreslaw.asp"
            >Moore’s Law</a
          >? But there’s change and then there’s a “big C” <em>Change</em> that
          would impact skills and potentially careers. The
          <a href="https://www.pluralsight.com/blog/career/tech-in-2025"
            >World Economic Forum, writing on the Pluralsight blog</a
          >, shared a worrisome observation about the future: “Across nearly all
          industries, the impact of technological and other changes is
          shortening the shelf-life of employees’ existing skill sets… ”
        </p>
        <p>
          So what skills will be sufficient to navigate the future? We asked 14
          <a href="/topics/devops/">DevOps</a> practitioners, analysts, and
          GitLab execs for their best advice.
        </p>
        <h2 id="embrace-the-soft-skills">Embrace the soft skills</h2>
        <p>
          In our 2020 <a href="/developer-survey/">Global DevSecOps Survey</a>,
          developers, security pros, ops team members, and testers were
          unanimous in their choice of the most important skills for the future:
          communication and collaboration. It’s not particularly surprising –
          DevOps team members are increasingly finding themselves working even
          more closely together and often in different or new areas of the
          company. Communication and collaboration in those cases can be the
          difference between success and failure.
        </p>
        <p>
          “You can’t have one brain that knows it all,” explains
          <a href="/company/team/#DarwinJS">Darwin Sanoy</a>, senior solutions
          architect, Americas, at GitLab. “You need communication and
          collaboration to work together.”
        </p>
        <p>
          One way developers can fine-tune collab skills is to use their open
          source skills within their organizations, a practice known as “inner
          sourcing,” says
          <a
            href="https://www.linkedin.com/in/jose-manrique-lopez-de-la-fuente-b869884/"
            >Jose Manrique Lopez de la Fuente</a
          >, CEO at Bitergia, and also a
          <a href="/community/heroes/">GitLab Hero</a>. “You’re not doing open
          source alone,” Manrique says. “There are hundreds of developers
          worldwide also doing it. So, with those skills I learned working with
          other developers, how can I be transparent with people who are not
          only connected to my team? How can I get more involved with what’s
          going on in the company?” The more developers practice this skill, the
          easier it will get, he predicts.
        </p>
        <h2 id="its-not-just-about-tech">It’s not just about tech</h2>
        <p>
          Although this seems counter-intuitive, future-proofing your career
          doesn’t necessarily mean boning up on new technologies. In
          <a href="/developer-survey/">our survey</a>, 28% of developers said
          <a
            href="https://www2.deloitte.com/us/en/insights/focus/signals-for-strategists/ai-assisted-software-development.html"
            >AI was an important skill to know for the future (and they’re
            probably not wrong)</a
          >, but most experts think it’s not wise to place all your energy in
          just a single specialty.
        </p>
        <p>
          “It’s best if you migrate your career from specialty to specialty
          trying to ride the wave,” Darwin says. “Take a look at what is picking
          up momentum but is not bleeding edge yet.” GitLab’s director of
          product management, CI/CD
          <a href="/company/team/#jyavorska">Jason Yavorska</a> suggests
          polishing up the basics. “You want solid tech skills like
          trouble-shooting, a current knowledge of modern stacks and a lot of
          basic things,” Jason explains. “You want to be a little bit more of a
          generalist than an expert in one field.”
        </p>
        <p>
          This is definitely a time to take step back and look at the bigger
          picture, suggests
          <a href="https://www.linkedin.com/in/philliplamb/">Philip Lamb</a>,
          global partner senior solutions architect – DevOps at Red Hat. He’s
          also a proponent of the power of generalization. “Focus less on
          specific tooling, software, and instead focus more on process and
          establishing a clear understanding of the sea changes DevOps brings,”
          he says. And don't forget that DevOps is going to look different for
          every organization.
        </p>
        <slp-click-to-tweet
          sourceUrl="https://current-page-url-here.com"
          tweetBody='"How software developers can future-proof their careers (and why it’s better to be a generalist than a specialist). 14 DevOps practitioners, analysts, and @gitlab execs weigh in"'
        />
        <h2 id="choose-wisely">Choose wisely</h2>
        <p>
          But if there’s one thing to keep in mind, above anything else, it’s
          this: “Avoid what AI is going to be good at,” Jason says. Forrester
          Research (and many others) think AI
          <a href="/blog/2020/10/28/ai-in-software-development/"
            >could be creating code in 10 years or less</a
          >. “AI and machine learning could be the most disrupting things to
          come to your career,” he explains. “If you’ve built your job out of
          basic things you could find yourself redundant. Focus on things you
          (as a human) are capable of.”
        </p>
        <slp-blog-merch-banner
          body="Learn to stabilize your work-from-home team and dive deep on topics including asynchronous workflows, meetings, informal communication, and management."
          cta="Get the eBook"
          destinationUrl="#"
          title="Free eBook: The GitLab Remote Playbook"
          class="slp-mx-auto slp-my-96"
        />
      </div>
    </template>
    <template v-slot:footer>
      <slp-tags
        class="slp-max-w-5xl slp-mx-auto slp-mb-20"
        :label="true"
        :links="tagsLinks"
      />
      <slp-blog-related-content :posts="relatedPosts" />
    </template>
    <template v-slot:aside>
      <div
        class="slp-bg-grayscale-gray2 slp-text-white slp-text-center slp-p-32"
      >
        This is a placeholder for the Marketo form
      </div>
      <slp-blog-merch-sidebar
        :title="merchSidebar.title"
        :cta="merchSidebar.cta"
        :destination-url="merchSidebar.destinationUrl"
        class="slp-hidden md:slp-block slp-my-auto"
      />
    </template>
  </slp-blog-layout>
</template>

<script>

export default {
  name: 'BlogPressureTest2',
  data() {
    return {
      breadcrumbs: {
        base: {
          link: '#',
          name: 'Blog',
        },
        category: {
          link: '#',
          name: 'Insights',
        },
      },
      tagsLinks: [
        {
          link: '#',
          text: 'Dev Ops',
        },
        {
          link: '#',
          text: 'Careers',
        },
        {
          link: '#',
          text: 'Testing',
        },
      ],
      header: {
        author: {
          avatarUrl: 'val-silv.png',
          link: '#',
          name: 'Valerie Silverthorne',
        },
        date: 'Oct 30, 2020',
        readTime: '6 min read',
        title: 'Future-proof your developer career',
      },
      merchSidebar: {
        title: 'Free eBook: The GitLab Remote Playbook',
        cta: 'Download Now',
        destinationUrl: '#',
      },
      relatedPosts: [
        {
          author: 'Chrissie Buchanan',
          avatarUrl: 'chrissie-buchanan.png',
          category: 'Insights',
          href: '#',
          title: "How we're improving migrations from Jenkins to GitLab CI/CD",
        },
        {
          author: 'Chrissie Buchanan',
          avatarUrl: 'chrissie-buchanan.png',
          category: 'Insights',
          href: '#',
          title:
            'How pre-filled CI/CD variables will make running pipelines easier',
        },
        {
          author: "Brendan O'Leary",
          avatarUrl: 'brendan-o-leary.png',
          category: 'Insights',
          href: '#',
          title: "CNCF's 5 technologies to watch in 2021",
        },
      ],
    };
  },
};
</script>
