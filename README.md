# nuxt-slippers-integration-demo

This is a demo repository to show how we could easily integrate the [slippers-ui](https://www.npmjs.com/package/slippers-ui) package with a [Nuxt.js](https://nuxtjs.org/) site.

## Steps: 

1. Add slippers-ui with `yarn add slippers-ui`
1. Add a Nuxt [plugin](https://nuxtjs.org/examples/plugins-vue) at `plugins/slippers-ui.js`
1. Register the plugin, in `nuxt.config.js`
1. Add additional global CSS from `slippers-ui`
1. Add a page at `pages/blog-pressure-test.vue`

This allows us to run `yarn dev` and visit `localhost:3000/blog-pressure-test` to see one of the Slippers blog templates in action. It's missing static assets like fonts and some images, so it may not match [the Storybook version]() completely, but it's a quick rundown on a very convenient way to develop the GitLab marketing site and Slippers in parallel. 

## Visualization 

You can view these steps as git diffs here: 

1. [First step, adding slippers-ui as a plugin](https://gitlab.com/tywilliams/nuxt.js-and-slippers-integration-demo/-/compare/660860c4...e1f53496?from_project_id=28474016)
1. [Second step, adding a demo page](https://gitlab.com/tywilliams/nuxt.js-and-slippers-integration-demo/-/compare/e1f53496...18826d76?from_project_id=28474016)


## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

## Special Directories

You can create the following extra directories, some of which have special behaviors. Only `pages` is required; you can delete them if you don't want to use their functionality.

### `assets`

The assets directory contains your uncompiled assets such as Stylus or Sass files, images, or fonts.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/assets).

### `components`

The components directory contains your Vue.js components. Components make up the different parts of your page and can be reused and imported into your pages, layouts and even other components.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/components).

### `layouts`

Layouts are a great help when you want to change the look and feel of your Nuxt app, whether you want to include a sidebar or have distinct layouts for mobile and desktop.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/layouts).


### `pages`

This directory contains your application views and routes. Nuxt will read all the `*.vue` files inside this directory and setup Vue Router automatically.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/get-started/routing).

### `plugins`

The plugins directory contains JavaScript plugins that you want to run before instantiating the root Vue.js Application. This is the place to add Vue plugins and to inject functions or constants. Every time you need to use `Vue.use()`, you should create a file in `plugins/` and add its path to plugins in `nuxt.config.js`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/plugins).

### `static`

This directory contains your static files. Each file inside this directory is mapped to `/`.

Example: `/static/robots.txt` is mapped as `/robots.txt`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/static).

### `store`

This directory contains your Vuex store files. Creating a file in this directory automatically activates Vuex.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/store).
